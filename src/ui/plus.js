import React from 'react';

const Plus = () => (
  <svg viewBox="0 0 91 90" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M54 36.5h32.5c2.20914 0 4 1.79086 4 4v9c0 2.20914-1.79086 4-4 4H54V86c0 2.20914-1.79086 4-4 4h-9c-2.20914 0-4-1.79086-4-4V53.5H4.5c-2.20914 0-4-1.79086-4-4v-9c0-2.20914 1.79086-4 4-4H37V4c0-2.20914 1.79086-4 4-4h9c2.20914 0 4 1.79086 4 4v32.5z"
      fillRule="evenodd"
    />
  </svg>
);

export default Plus;
