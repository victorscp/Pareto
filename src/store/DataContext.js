import React, { Component } from 'react';
import uuidv4 from 'uuid/v4';

import { todo, doing, done } from './../data';

export const DataContext = React.createContext();

class DataProvider extends Component {
  state = {
    colunas: {
      todo: [...todo],
      doing: [...doing],
      done: [...done]
    },
    todoNew: false,
    doingNew: false,
    doneNew: false,
    novaColunaTexto: '',
    novoTodoTitulo: '',
    novoTodoTags: []
  };

  applyDrag = (arr, dragResult) => {
    const { removedIndex, addedIndex, payload } = dragResult;
    if (removedIndex === null && addedIndex === null) return arr;

    const result = [...arr];
    let itemToAdd = payload;

    if (removedIndex !== null) {
      itemToAdd = result.splice(removedIndex, 1)[0];
    }

    if (addedIndex !== null) {
      result.splice(addedIndex, 0, itemToAdd);
    }

    return result;
  };

  exchangeTodo = (todo, e) => {
    this.setState({
      colunas: {
        ...this.state.colunas,
        [todo]: this.applyDrag(this.state.colunas[todo], e)
      }
    });
  };

  toggleTodo = todo => {
    this.setState({ [`${todo}New`]: !this.state[`${todo}New`] });
  };

  addNewTodo = (coluna, e) => {
    e.preventDefault();
    let parsedTags = [];
    const newID = uuidv4().toString();
    const { novoTodoTitulo, novoTodoTags } = this.state;

    if (novoTodoTags.length > 0) {
      const splitTags = novoTodoTags.replace(' ', '').split(',');
      parsedTags = [...splitTags];
    }

    if (novoTodoTitulo && novoTodoTitulo.length > 0) {
      this.setState({
        colunas: {
          ...this.state.colunas,
          [coluna]: [
            ...this.state.colunas[coluna],
            { id: newID, nome: novoTodoTitulo, tags: parsedTags }
          ]
        },
        [`${coluna}New`]: false,
        novoTodoTitulo: '',
        novoTodoTags: []
      });
    }
  };

  addNewColuna = e => {
    e.preventDefault();

    const parsedNome = this.state.novaColunaTexto
      .replace(' ', '')
      .toLowerCase();

    this.setState({
      colunas: { ...this.state.colunas, [parsedNome]: [] },
      [`${parsedNome}New`]: false,
      novaColunaTexto: ''
    });
  };

  changeInputValue = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  render() {
    return (
      <DataContext.Provider
        value={{
          state: this.state,
          exchangeTodo: this.exchangeTodo,
          toggleTodo: this.toggleTodo,
          addNewTodo: this.addNewTodo,
          addNewColuna: this.addNewColuna,
          changeInputValue: this.changeInputValue
        }}>
        {this.props.children}
      </DataContext.Provider>
    );
  }
}

export default DataProvider;
