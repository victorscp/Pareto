import uuidv4 from 'uuid/v4';

export const todo = [
  { id: uuidv4(), nome: 'Tenho que fazer algo.', tags: ['tag', 'outra tag'] }
];

export const doing = [
  { id: uuidv4(), nome: 'Fazendo essa parada...', tags: [] }
];

export const done = [{ id: uuidv4(), nome: 'Acabei! Show!', tags: [] }];
