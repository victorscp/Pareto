import React from 'react';
import styled from 'react-emotion';

import { DataContext } from './../store/DataContext';

const Form = styled('form')`
  width: 100%;
  align-items: center;
  background: #ecebe9;
  border-top: 1px solid #c1bcb7;
  display: flex;
  justify-content: space-between;
  padding: 20px;
  position: absolute;
  top: 47px;
  transition: all 0.25s ease-in-out;
  z-index: 4;

  &.slide-enter {
    opacity: 0;
    transform: translateY(-30px);
  }

  &.slide-enter.slide-enter-active {
    opacity: 1;
    transform: translateY(0);
  }

  &.slide-exit {
    opacity: 0;
    transform: translateY(-30px);
  }
`;

const Input = styled('input')`
  width: 100%;
  height: 45px;
  background: white;
  border: none;
  border-radius: 4px;
  padding: 8px;

  &:not(:last-of-type) {
    margin-bottom: 10px;
  }
`;

const Button = styled('button')`
  width: 45px;
  height: 45px;
  align-items: center;
  background: #c1bcb7;
  border: none;
  border-radius: 100px;
  color: white;
  cursor: pointer;
  display: flex;
  font-size: 25px;
  justify-content: center;
  padding: 10px 8px;

  &:hover {
    background: #9ac286;
  }
`;

const NewTodo = props => (
  <DataContext.Consumer>
    {({ state, addNewTodo, changeInputValue }) => (
      <Form onSubmit={e => addNewTodo(props.coluna, e)}>
        <div style={{ width: 'calc(100% - 55px)' }}>
          <Input
            type="text"
            placeholder="Título"
            value={state.novoTodoTitulo}
            name="novoTodoTitulo"
            onChange={e => changeInputValue(e)}
          />
          <Input
            type="text"
            placeholder="Tags separadas por vírgulas..."
            value={state.novoTodoTags}
            name="novoTodoTags"
            onChange={e => changeInputValue(e)}
          />
        </div>
        <Button>✔</Button>
      </Form>
    )}
  </DataContext.Consumer>
);

export default NewTodo;
