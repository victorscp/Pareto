import React from 'react';
import styled from 'react-emotion';
import uuidv4 from 'uuid/v4';

const TodoStyled = styled('div')`
  width: 100%;
  background: #f8f3e4;
  border: 1px solid #ebdfb6;
  font-weight: 700;
  margin-top: 10px;
  padding: 10px;
`;

const Tags = styled('div')`
  border-top: 1px dashed #c1bcb7;
  margin-top: 10px;
  padding-top: 10px;

  span {
    background: white;
    border-radius: 4px;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.05);
    font-size: 14px;
    padding: 3px 8px;

    &:not(:last-of-type) {
      margin-right: 8px;
    }
  }
`;

const Todo = props => (
  <TodoStyled>
    {props.children}
    {props.tags.length > 0 ? (
      <Tags>{props.tags.map(tag => <span key={uuidv4()}>{tag}</span>)}</Tags>
    ) : null}
  </TodoStyled>
);

export default Todo;
