import React, { Component } from 'react';
import styled from 'react-emotion';

import { DataContext } from './../store/DataContext';

const Form = styled('form')`
  width: 200px;
  align-items: center;
  border-top: 1px solid #c1bcb7;
  display: flex;
  justify-content: space-between;
  position: absolute;
  top: 10px;
  left: 50%;
  transform: translateX(-50%);

  @media (min-width: 800px) {
    left: unset;
    right: 10px;
    transform: unset;
  }
`;

const Input = styled('input')`
  width: 100%;
  height: 30px;
  background: white;
  border: none;
  border-radius: 4px;
  padding: 8px;

  &:not(:last-of-type) {
    margin-bottom: 10px;
  }
`;

const Button = styled('button')`
  width: 30px;
  height: 30px;
  align-items: center;
  background: white;
  border: none;
  border-radius: 100px;
  color: #c1bcb7;
  cursor: pointer;
  display: flex;
  font-size: 20px;
  justify-content: center;
  padding: 0;

  &:hover {
    background: #9ac286;
    color: white;
  }
`;

class NewColuna extends Component {
  render() {
    return (
      <DataContext.Consumer>
        {({ state, changeInputValue, addNewColuna }) => (
          <Form onSubmit={e => addNewColuna(e)}>
            <div style={{ width: 'calc(100% - 40px)' }}>
              <Input
                type="text"
                placeholder="Nova coluna..."
                value={state.novaColunaTexto}
                name="novaColunaTexto"
                onChange={e => changeInputValue(e)}
              />
            </div>
            <Button>✔</Button>
          </Form>
        )}
      </DataContext.Consumer>
    );
  }
}

export default NewColuna;
