import React, { Component, Fragment } from 'react';
import styled from 'react-emotion';
import { Container, Draggable } from 'react-smooth-dnd';

import { DataContext } from './../store/DataContext';
import Board from './Board';
import Todo from './Todo';
import NewColuna from './NewColuna';

const AppStyle = styled('div')`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  padding: 55px 40px 40px 40px;

  @media (min-width: 800px) {
  }

  @media (min-width: 1000px) {
    padding: calc(7vw + 10px);
  }
`;

class App extends Component {
  render() {
    return (
      <AppStyle>
        <DataContext.Consumer>
          {({ state, exchangeTodo }) => (
            <Fragment>
              <NewColuna />
              {Object.keys(state.colunas).map(coluna => {
                return (
                  <Board key={coluna} nome={coluna}>
                    <Container
                      style={{ height: '100%', overflowY: 'auto' }}
                      groupName="coluna"
                      getChildPayload={index => state.colunas[coluna][index]}
                      onDrop={e => exchangeTodo(coluna, e)}>
                      {state.colunas[coluna].map(todo => (
                        <Draggable key={todo.id}>
                          <Todo tags={todo.tags}>{todo.nome}</Todo>
                        </Draggable>
                      ))}
                    </Container>
                  </Board>
                );
              })}
            </Fragment>
          )}
        </DataContext.Consumer>
      </AppStyle>
    );
  }
}

export default App;
