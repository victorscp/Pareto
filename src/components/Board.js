import React from 'react';
import styled from 'react-emotion';
import { CSSTransition } from 'react-transition-group';

import { DataContext } from './../store/DataContext';
import Plus from './../ui/plus';
import NewTodo from './NewTodo';

const BoardStyle = styled('div')`
  width: 100%;
  height: 50%;
  max-height: 100%;
  background: white;
  border-radius: 5px;
  box-shadow: 0 4px 10px rgba(0, 0, 0, 0.1);
  margin-bottom: 30px;
  overflow: hidden;
  position: relative;

  @media (min-width: 800px) {
    width: calc(${props => props.widthColuna} - 30px);
    height: 100%;
    margin-bottom: 0;
  }
`;

const Titulo = styled('div')`
  width: 100%;
  height: 47px;
  align-items: center;
  background: #ecebe9;
  display: flex;
  font-size: 14px;
  font-weight: 900;
  justify-content: center;
  position: relative;
  text-transform: uppercase;

  h2 {
    font-weight: inherit;
    margin: 0;
  }
`;

const AddTodo = styled('button')`
  width: 35px;
  height: 35px;
  align-items: center;
  border: none;
  border-radius: 100px;
  background: ${props => (props.ativo ? '#CF8585' : 'white')};
  cursor: pointer;
  display: flex;
  justify-content: center;
  padding: 0;
  position: absolute;
  right: 10px;
  transform: ${props => (props.ativo ? 'rotate(-45deg)' : 'rotate(0)')};

  svg {
    width: 19px;
    height: 19px;
    fill: ${props => (props.ativo ? 'white' : '#9ac286')};
    display: inline-block;
    transition: all 0.25s ease-in-out;
  }

  &:hover {
    background: ${props => (props.ativo ? '#CF8585' : '#9ac286')};

    svg {
      fill: white;
    }
  }
`;

const Content = styled('div')`
  height: calc(100% - 47px);
  overflow-y: auto;
  padding: 10px 20px 20px 20px;
`;

const Board = props => (
  <DataContext.Consumer>
    {({ state, toggleTodo }) => (
      <BoardStyle widthColuna={`${100 / Object.keys(state.colunas).length}%`}>
        <Titulo>
          <h2>{props.nome}</h2>

          <AddTodo
            ativo={!!state[`${props.nome}New`]}
            onClick={() => toggleTodo(props.nome)}>
            <Plus />
          </AddTodo>
        </Titulo>

        <CSSTransition
          in={state[`${props.nome}New`]}
          timeout={300}
          classNames="slide"
          unmountOnExit>
          <NewTodo coluna={props.nome} />
        </CSSTransition>
        <Content>{props.children}</Content>
      </BoardStyle>
    )}
  </DataContext.Consumer>
);

export default Board;
